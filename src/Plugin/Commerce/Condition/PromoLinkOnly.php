<?php

namespace Drupal\commerce_promo_link\Plugin\Commerce\Condition;

use Drupal\commerce\Plugin\Commerce\Condition\ConditionBase;
use Drupal\commerce\Plugin\Commerce\Condition\ParentEntityAwareInterface;
use Drupal\commerce\Plugin\Commerce\Condition\ParentEntityAwareTrait;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a condition to limit promotion usage to the link.
 *
 * @CommerceCondition(
 *   id = "promo_link_only",
 *   label = @Translation("Promo Link restrictions"),
 *   category = @Translation("Order"),
 *   entity_type = "commerce_order",
 *   parent_entity_type = "commerce_promotion",
 *   weight = 99,
 * )
 */
class PromoLinkOnly extends ConditionBase implements ContainerFactoryPluginInterface, ParentEntityAwareInterface {

  use ParentEntityAwareTrait;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'link_only' => FALSE,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['container'] = [
      '#type' => 'fieldset',
    ];
    $form['container']['link_only'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Can only be applied using the promocode link.'),
      '#default_value' => $this->configuration['link_only'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);
    $this->configuration['link_only'] = $values['container']['link_only'];
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(EntityInterface $entity) {
    $this->assertEntity($entity);
    /** @var \Drupal\commerce_order\Entity\OrderInterface $order */
    $order = $entity;
    $code = $order->getData('commerce_promo_link_promotion_code');
    if ($code) {
      /** @var \Drupal\commerce_promotion\CouponStorageInterface $storage */
      $storage = $this->entityTypeManager->getStorage('commerce_promotion_coupon');
      $coupon = $storage->loadEnabledByCode($code);
      return $coupon && $coupon->available($order);
    }
    return FALSE;
  }

}
