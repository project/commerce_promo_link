<?php

namespace Drupal\commerce_promo_link\Controller;

use Drupal\commerce_promotion\Entity\CouponInterface;
use Drupal\commerce_promotion\PromotionUsage;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TempStore\PrivateTempStoreFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Provides route responses for commerce promo link module.
 */
class PromoLink extends ControllerBase {

  use StringTranslationTrait;
  use MessengerTrait;

  /**
   * Controller to handle the promotion link.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The service container.
   * @param \Drupal\commerce_store\CurrentStoreInterface $current_store
   *   The current store.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\TempStore\PrivateTempStoreFactory $temp_store_factory
   *   The private temp store.
   * @param \Drupal\commerce_promotion\PromotionUsage $promotion_usage
   *   The promotion usage service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    protected ContainerInterface $container,
    protected CurrentStoreInterface $current_store,
    protected AccountInterface $current_user,
    protected PrivateTempStoreFactory $temp_store_factory,
    protected PromotionUsage $promotion_usage,
    protected EntityTypeManagerInterface $entity_type_manager
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container,
      $container->get('commerce_store.current_store'),
      $container->get('current_user'),
      $container->get('tempstore.private'),
      $container->get('commerce_promotion.usage'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Validates and stores the coupon code to the temp storage.
   *
   * @param string $code
   *   The promotion code/
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect to front.
   */
  public function addPromoCode(string $code): RedirectResponse {

    // Force session start if we don't already have a session.
    $session = $this->container->get('session');
    if (!$session->isStarted()) {
      $session->migrate();
    }

    $code = Html::escape($code);
    /** @var \Drupal\commerce_promotion\CouponStorageInterface $couponStorage */
    $couponStorage = $this->entity_type_manager->getStorage('commerce_promotion_coupon');
    $coupon = $couponStorage->loadEnabledByCode($code);
    // Check if coupon exists.
    if (empty($coupon)) {
      $this->messenger()->addError($this->t('The provided coupon code is invalid.'));
      return $this->redirect('<front>');
    }
    // Check if coupon is expired not yet started or used.
    if (!$this->couponIsExpiredOrUsed($coupon)) {
      $this->messenger()->addError($this->t('The provided coupon code is not available, it may have expired or have already been used.'));
      return $this->redirect('<front>');
    }
    $this->temp_store_factory->get('commerce_promo_link')->set('coupon_code', $code);
    $this->messenger()->addMessage($this->t('Your coupon code @code will be automatically applied during the checkout.', ['@code' => $code]));
    return $this->redirect('<front>');
  }

  /**
   * Check if coupon is expired not yet started or used.
   *
   * @param \Drupal\commerce_promotion\Entity\CouponInterface $coupon
   *   The coupon entity.
   *
   * @return bool
   *   True if valid false if invalid.
   */
  protected function couponIsExpiredOrUsed(CouponInterface $coupon): bool {
    $couponUsage = $this->promotion_usage->loadByCoupon($coupon);
    $promotion = $coupon->getPromotion();
    $promotionUsage = $this->promotion_usage->load($promotion);
    if ($coupon->getUsageLimit() && $couponUsage >= $coupon->getUsageLimit()) {
      return FALSE;
    }
    if ($promotion->getUsageLimit() && $promotionUsage >= $promotion->getUsageLimit()) {
      return FALSE;
    }
    $startDate = $promotion->getStartDate()?->getPhpDateTime();
    $endDate = $promotion->getEndDate()?->getPhpDateTime();
    $currentDate = new \DateTime('now', new \DateTimeZone('UTC'));
    if (!empty($startDate) && $currentDate->getTimestamp() < $startDate->getTimestamp()) {
      return FALSE;
    }
    if (!empty($endDate) && $currentDate->getTimestamp() > $endDate->getTimestamp()) {
      return FALSE;
    }
    $couponStartDate = $coupon->getStartDate()?->getPhpDateTime();
    $couponEndDate = $coupon->getEndDate()?->getPhpDateTime();
    if (!empty($couponStartDate) && $currentDate->getTimestamp() < $couponStartDate->getTimestamp()) {
      return FALSE;
    }
    if (!empty($couponEndDate) && $currentDate->getTimestamp() > $couponEndDate->getTimestamp()) {
      return FALSE;
    }
    return TRUE;
  }

}
